var express = require ("express");

var app = express();

app.use(express.static(__dirname + "/public"));

//Process request
app.get("/time", function(req, res){
    var currTime = new Date();
    
    //set status code
    res.status(200);
    
    //set the content type - MIME type **content type is very impt - controls what browser sees
    res.type("text/html");
    
    //custom header
    res.set("X-My-Header", "hello");
    
    //send the result
    res.send("<h1>The current time is: " + currTime + "</h1>");
});

var tennis = [
    'wimbeldon-2015.jpg', 'wimbledon.jpg','wimbledon-2015-sunday-2-federer-1.jpg'
];

app.get('/tennis', function(req, res){
    res.status(200);
    res.type('text/html');
    var idx = Math.floor(Math.random() * tennis.length);
    res.send("<img src = 'xxx/" + tennis[idx] + "'>"
    + "<h2>" + tennis[idx] + "</h2>")
});

app.get('/pagetwo', function (req, res){
    res.redirect('/image/image.html');
});

app.get("/xxx/:imgname", function(req, res){
    res.sendFile(req.params.imgname,{
        root: __dirname + '/public/image/'
    });
});
app.get("/yyy/:imgname", function(req, res){
    res.send('<img src="' + '/image/' + req.params.imgname + '"><br>' +
        req.params.imgname);
});



app.use(function(req,res,next){
    console.info("incoming request: %s", req.originalUrl);
    next();
});

app.use(function(req,res){
    console.info("File not found in public: %s", req.originalUrl);
    res.redirect("/error.html");
});

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get("port"),function(){
    console.info("Application started on port %d", app.get("port"));
});